<?php
    session_start();

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "form_database";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password,$dbname);

    // Check connection
    if ($conn->connect_error){
        die("Connection failed: " .$conn->connect_error);
    }

    $sql = "SELECT * FROM register_details";
    $result = $conn->query($sql);
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Page Title</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
        <script src="main.js"></script>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>

    <body>

        <style>

            body{
                background:url(https://i.pinimg.com/originals/6f/47/89/6f478954566cbabf6b213e908e5c9a78.png); 
                background-size:100%;
            }

            .custab{
                border: 1px solid #ccc;
                padding: 5px;
                margin: 5px;
                box-shadow: 3px 3px 2px #ccc;
                transition: 0.5s;
                
            }

            .custab:hover{          
                box-shadow: 3px 3px 0px transparent;
                transition: 0.5s;
            }

        </style>

        <div class="container" style="width:100%;">
        
            <div class="row col-md-6 col-md-offset-2 custyle" style="width:100%; margin: 5px;">

            <label style="text-align:center; color:white;   "><h2>User Details Table</h2></label>

                <table class="table table custab" style="color:white;">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Email ID</th>
                            <th>Phone Number</th>
                            <th>User Option</th>
                            <th>Zip Code</th>
                            <th>Money</th>
                            <th>Website</th>
                            <th>Comment</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>


                    <?php 
                    
                    if ($result->num_rows > 0) {
                        
                        $html="";
        
                        while($row = $result->fetch_assoc()){
                
                            $html.='<tr> 
                            <td>'.$row["id"].'</td>
                            <td>'.$row["username"].'</td>
                            <td>'.$row["age"].'</td>
                            <td>'.$row["email"].'</td>
                            <td>'.$row["phone_number"].'</td>
                            <td>'.$row["user_option"].'</td>
                            <td>'.$row["zip_code"].'</td>
                            <td>'.$row["money"].'</td>
                            <td>'.$row["website"].'</td>
                            <td>'.$row["comment"].'</td>
                            <td class="text-center">
                                
                                <a class="btn btn-info btn-xs" href="Userpage.php?id='.$row["id"].'">
                                <span class="glyphicon glyphicon-edit"></span> View</a> 

                                <a class="btn btn-info btn-xs" href="Edit.php?id='.$row["id"].'">
                                <span class="glyphicon glyphicon-edit"></span> Edit</a> 

                                <a href="Delete.php?id='.$row["id"].'" class="btn btn-danger btn-xs">
                                <span class="glyphicon glyphicon-remove"></span> Del</a>

                            </td>
                            ';
                        }
                
                        $html.='</tr>';
                
                        echo $html;
                    } 

                    $conn->close();
                    
                    ?>

                </table>

            </div>

        </div>

    </body>
    
</html>