<?php 

    session_start();

    if(isset($_POST['submit'])){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "form_database";

        // Create connection
        $conn = mysqli_connect($servername, $username, $password,$dbname);

        // Check connection
        if ($conn->connect_error){
            die("Connection failed: " .$conn->connect_error);
        }   

        $user_name = $_POST['username'];
        $user_password = $_POST['password'];
        $confirm_password = $_POST['confirm_password'];
        $user_email = $_POST['email'];
        $phone_number = $_POST['phone'];
        $zip_code = $_POST['zipcode'];
        $website = $_POST['website'];
        $dob= $_POST['dob'];

        $sql = "SELECT * FROM register_details where email = '".$_POST['email']."' OR username = '".$_POST['username']."'";
        $result = $conn->query($sql);

        if(!($result->num_rows > 0)){
            
            $error_array=array();
            $error = 0;

            $username_pattern = '/^[a-zA-Z0-9]{6,}$/';
            if(!preg_match($username_pattern, $user_name)) { 
                // valid username, alphanumeric & longer than or equals 5 chars
                $error_array['error_username'] = "Username must contain min. 5 letters and a number";
                $error = 1;

            }

            $password_pattern = '/^(?=.*\d)(?=.*[A-Za-z])(?=.*[!@#$%])[0-9A-Za-z!@#$%]{6,15}$/';
            if(!preg_match($password_pattern,$user_password)){
                $error_array['error_password'] = "Password must contain 6 digits, a number and a special character";
                $error = 1;
            }

            if($_POST["password"] !== $_POST["confirm_password"]){
                $error_array['error_confirm_pwd'] = "Passwords doesnot Match";
                $error = 1;
            }

            $emailpattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
            if(!preg_match($emailpattern,$user_email)){
                
                $error_array['error_email'] = "Please enter the valid email ID";
                $error = 1;
            }

            if(!preg_match('/^\d{10}$/',$phone_number)){
                $error_array['error_phone'] = "Invalid Phone Number";
                $error = 1;
            }

            if(!preg_match('/^\d{6}$/',$zip_code)){
                $error_array['error_zipcode'] = "Invalid Zip Code";
                $error = 1;
            }

            if($error == 0){

                $sql = "INSERT INTO register_details (username,password,cnfm_pwd,age,email,phone_number,user_option,zip_code,money,website,dob,comment)   
                VALUES ('".$user_name."','".$user_password."','".$confirm_password."','".$diff."','".$user_email."','".$phone_number."','".$_POST['selected_option']."','".$zip_code."','".$_POST['money']."','".$website."','".$_POST['dob']."','".$_POST['comment']."')";
                $result = $conn->query($sql);

                $id = mysqli_insert_id($conn);
                
                $conn->close();

                if($result){
                    
                    header("location:Records.php");
                }
                
            }
            else{
                $_SESSION['error_msgs'] = $error_array;
            }

        }
        else{

            $error_array['email_exist'] = "Email / Username  already exists";
            
        }

    }


    if(isset($_SESSION['error_msgs']) && !empty($_SESSION['error_msgs'])){
        $err_array = $_SESSION['error_msgs'];

        session_destroy();
    }

?>

<html lang="en">

    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Website CSS style -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="style.css">
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	</head>

	<body>
		<div class="container">

			<div class="row main">

				<div class="main-login main-center">

				<h3 style="text-align:center">Register</h3>

					<form enctype="multipart/form-data" method="post" action="">

                        <span>
                            <?php 
                                if(isset($err_array["err_msg"]) && !empty($err_array)){
                                    echo "<span style='color:red'>* ".$err_array["err_msg"]."</span>";
                                }
                            ?>
                        </span>

                        <div class="form-group">
							
                            <label for="username" class="cols-sm-2 control-label">Username</label>
							
                            <div class="cols-sm-10">
								<div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Enter your Username" value="<?php if(isset($_POST['username']) && !empty($_POST)){ echo $_POST['username']; } ?>"/>
								</div>

                                <span>
                                    <?php 
                                        if(isset($err_array["error_username"]) && !empty($err_array)){
                                            echo "<span style='color:red'>* ".$err_array["error_username"]."</span>";
                                        }
                                    ?>
                                </span>

							</div>

                        </div>
                        
                        <div class="form-group">

							<label for="password" class="cols-sm-2 control-label">Password</label>

							<div class="cols-sm-10">

								<div class="input-group">
                                    
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="password" id="user_password" placeholder="Enter your Password" />

								</div>

                                 <span>
                                    <?php 
                                        if(isset($err_array["error_password"]) && !empty($err_array)){
                                            echo "<span style='color:red'>* ".$err_array["error_password"]."</span>";
                                        }
                                    ?>
                                </span>

							</div>
                            
                        </div>
                        
                        <div class="form-group">

							<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>

							<div class="cols-sm-10">

								<div class="input-group">
                                    
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="confirm_password" id="confirm_pwd"  placeholder="Confirm your Password" />

								</div>

                                <span>
                                    <?php 
                                        if(isset($err_array["error_confirm_pwd"]) && !empty($err_array)){
                                            echo "<span style='color:red'>* ".$err_array["error_confirm_pwd"]."</span>";
                                        }
                                    ?>
                                </span>

							</div>

						</div>  
						
						<div class="form-group">

							<label for="name" class="cols-sm-2 control-label">Age</label>

							<div class="cols-sm-10">

								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-heart fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="age" id="user_age"  placeholder="Your Age" value="" readonly/>
								</div>

							</div>

						</div>

						<div class="form-group">

							<label for="email" class="cols-sm-2 control-label">Your Email</label>

							<div class="cols-sm-10">

								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email" value="<?php if(isset($_POST['email']) && !empty($_POST)){ echo $_POST['email']; } ?>"/>
                                </div>

                                <span>
                                    <?php 
                                        if(isset($err_array["error_email"]) && !empty($err_array)){
                                            echo "<span style='color:red'>* ".$err_array["error_email"]."</span>";
                                        }
                                    ?>
                                </span>

							</div>
                        </div>
                        
                        <div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Phone Number</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
									<input type="number" class="form-control" name="phone" id="user_phone"  placeholder="Enter your Phone Number"  value="<?php if(isset($_POST['phone']) && !empty($_POST)){ echo $_POST['phone']; } ?>"/>
                                </div>

                                <span>
                                    <?php 
                                        if(isset($err_array["error_phone"]) && !empty($err_array)){
                                            echo "<span style='color:red'>* ".$err_array["error_phone"]."</span>";
                                        }
                                    ?>
                                </span>

							</div>
                        </div>
                        
                        <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">Select Your Option</label>

							<div class="cols-sm-10">

								<div class="input-group" id="dropdownoption">

                                    <select class="form-control" name="selected_option">
                                        <option class="option" selected disabled>Select Option</option>
                                        <option class="option" value="Student">Student</option>
                                        <option class="option" value="Professional">Professional</option>
                                    </select>

								</div>
							</div>

                        </div>
                        
                        <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">Your Zip Code</label>

							<div class="cols-sm-10">

								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-map-marker fa" aria-hidden="true"></i></span>
									<input type="number" class="form-control" name="zipcode" id="user_zipcode"  placeholder="Enter your Zip Code" value="<?php if(isset($_POST['zipcode']) && !empty($_POST)){ echo $_POST['zipcode']; } ?>"/>
                                </div>

                                <span>
                                    <?php 
                                        if(isset($err_array["error_zipcode"]) && !empty($err_array)){
                                            echo "<span style='color:red'>* ".$err_array["error_zipcode"]."</span>";
                                        }
                                    ?>
                                </span>

							</div>
						</div>

                        <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">Money</label>

							<div class="cols-sm-10">

								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-dollar fa" aria-hidden="true"></i></span>
									<input type="number" step=0.01 class="form-control" name="money" id="user_money"  placeholder="Enter your Money"  value="<?php if(isset($_POST['money']) && !empty($_POST)){ echo $_POST['money']; } ?>"/>
								</div>

							</div>

                        </div>
                        
                        <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">Your Birthday</label>

							<div class="cols-sm-10">

								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-birthday-cake fa" aria-hidden="true"></i></span>
									<input type="date" class="form-control" nama fa-birthday-cake fa name="dob" id="dob"  placeholder="Enter your Birthday" value="<?php if(isset($_POST['dob']) && !empty($_POST)){ echo $_POST['dob']; } ?>"/>
								</div>

							</div>

                        </div>
                        
                        <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">Your Website</label>

							<div class="cols-sm-10">

								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-external-link-square fa" aria-hidden="true"></i></span>
									<input type="url" class="form-control" name="website" id="user_website" placeholder="https://www.google.com/"  value="<?php if(isset($_POST['website']) && !empty($_POST)){ echo $_POST['website']; } ?>"/>
								</div>

							</div>

                        </div>
                        
                        <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">Your Comment</label>

							<div class="cols-sm-10">

								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-comments fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="comment" id="user_comment"  placeholder="Enter your Website"  value="<?php if(isset($_POST['comment']) && !empty($_POST)){ echo $_POST['comment']; } ?>"/>
								</div>

							</div>

						</div>
						
						<div class="form-group">
							<button type="submit" name="submit" id="button" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                        </div>
                        
                        <div class="form-group">
							<a href="Records.php" class="btn btn-primary btn-lg btn-block login-button">Show Records</a>
						</div>
						
					</form>

				</div>
                
			</div>

		</div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="main.js"></script>

    </body>
    
</html>