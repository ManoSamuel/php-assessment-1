<?php

    if(isset($_GET['id']) && !empty($_GET['id'])){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "form_database";

        // Create connection
        $conn = mysqli_connect($servername, $username, $password,$dbname);

        // Check connection
        if ($conn->connect_error){
            die("Connection failed: " .$conn->connect_error);
        }
        
        $id = $_GET['id'];

        $sql = "SELECT * FROM register_details where id = '".$id."'";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();

        session_start();
            
        if(isset($_POST['submit'])){

            $error_array=array();
            $user_name = $_POST['user_name'];
            $user_email = $_POST['user_email'];
            $phone_number = $_POST['user_phone'];
            $error = 0;

            $emailpattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
            if(!preg_match($emailpattern,$user_email)){
                
                $error_array['email'] = "Please enter the valid email ID";
                $error = 1;
            }

            if(!preg_match('/^\d{10}$/',$phone_number)){
                $error_array['phone'] = "Invalid Phone Number";
                $error = 1;
            }

            if($error == 0){

                $sql = "UPDATE register_details 
                    SET  
                    username = '".$_POST['user_name']."',
                    password = '".$_POST['user_password']."',
                    email = '".$_POST['user_email']."',
                    phone_number = '".$_POST['user_phone']."',
                    dob = '".$_POST['dob']."',
                    zip_code = '".$_POST['zip_code']."',
                    money = '".$_POST['money']."',
                    website = '".$_POST['website']."',
                    comment = '".$_POST['comment']."'
                    where id = '".$id."'
                ";

                $result = $conn->query($sql);

                if($result){
                    header("location:Records.php");
                }
            }
            else{
                $_SESSION['error_msgs'] = $error_array;
            }
               
        }

        if(isset($_SESSION['error_msgs']) && !empty($_SESSION['error_msgs'])){
            $err_array = $_SESSION['error_msgs'];
            
        }

    }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
        <script src="main.js"></script>
        <link rel="stylesheet" href="main1.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>

    <body>

        <div id="fullscreen_bg" class="fullscreen_bg">
            
            <form class="form-signin" enctype="multipart/form-data" method="post" action="">

                <div class="container">

                    <div class="row">

                        <div class="panel panel-default">

                            <div class="panel-body">
                                    
                                <div id="editpage">

                                    <div class="container">

                                        <div class="row">
                                            
                                            <form class="form-horizontal" role="form" method="POST" action="">

                                                <div class="row">
                                                    <div class="col-md-3"></div>
                                                    <div class="col-md-6">
                                                        <h2>Edit Profile</h2>
                                                        <hr>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="name">Name</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                                                                <input type="text" name="user_name" class="form-control" id="user_" value="<?php echo $row['username'] ?>" required autofocus>
                                                                <input type="hidden" name="hidden_id" class="form-control" id="user_id" value="<?php echo $row['id'] ?>" >
                            
                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="name">Password</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                                                                <input type="password" name="user_password" class="form-control" id="user_password" value="<?php echo $row['password'] ?>" required autofocus>
                                                            
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="email">E-Mail ID</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
                                                                
                                                                <input type="text"  name="user_email" class="glyphicon glyphicon-envelope form-control" id="email" value="<?php echo $row['email'] ?>" required autofocus>
                                                                
                                                                <span>
                                                                    <?php 
                                                                        if(isset($err_array["email"]) && !empty($err_array)){
                                                                            echo "<span style='color:red'>* ".$err_array["email"]."</span>";
                                                                        }
                                                                    ?>
                                                                </span>

                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="password">Phone Number</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group has-danger">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                                                                <input type="text" name="user_phone" class="glyphicon glyphicon-phone form-control" id="phone" value="<?php echo $row['phone_number'] ?>">
                                                            
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="name">Zip Code</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                                                                <input type="text" name="zip_code" class="form-control" id="user_zipcode" value="<?php echo $row['zip_code'] ?>" required autofocus>
                                    
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>


                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="name">Money</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                                                                <input type="text" name="money" class="form-control" id="user_" value="<?php echo $row['money'] ?>" required autofocus>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>


                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="password">Date Of Birth</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group has-danger">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                                                                <input type="date" name="dob" class=" form-control" id="phone" value="<?php echo $row['dob'] ?>">
                                                            
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="name">Website</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                                                                <input type="text" name="website" class="form-control" id="user_" value="<?php echo $row['website'] ?>" required autofocus>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3 field-label-responsive">
                                                        <label for="name">Comment</label>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                                                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                                                                <input type="text" name="comment" class="form-control" id="user_" value="<?php echo $row['comment'] ?>" required autofocus>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>


                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <button href="Record.php?id=<?php echo $row["id"]; ?>" type="submit" name="submit" class="btn btn-success"><i class="fa fa-user-plus"></i> Submit</button>
                                                    </div>

                                                </div>

                                            </form>

                                        </div>

                                    </div>

                                </div>
                                
                            </div>

                        </div>

                    </div>

                </div>

            </form>

        </div> 

    </body>
    
</html>`